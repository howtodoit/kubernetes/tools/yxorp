# Yxorp

**Yxorp** is based on [nginx official image](https://hub.docker.com/_/nginx) to work as reverse proxy. It was build to be easy to configure and use since you have specify just three environment variables.

**Yxorp** can be useful, for example, to expose all ingress route from minikube outside local machine.

## Basic usage with minikube

```shell
docker run -d \
    --rm \
    --env-file env \
    --network host \
    --name yxorp \
    registry.gitlab.com/howtodoit/kubernetes/tools/yxorp:latest
```

`env` example file

```
LISTEN='80'
SERVER_NAME='bfbf1a91.nip.io'
PROXY_PASS='http://<MINIKUBE_IP>:80'
```

You can find `MINIKUBE_IP` thru

```shell
minikube ip
```


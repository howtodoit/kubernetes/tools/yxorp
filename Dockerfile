FROM nginx:latest

ENV DOLLAR='$'
ENV LISTEN='80'
ENV SERVER_NAME='localhost'
ENV PROXY_PASS='http://127.0.0.1:80'

COPY default.conf.tpl /default.conf.tpl
COPY compile-template.sh /docker-entrypoint.d/compile-template.sh

RUN chmod +x /docker-entrypoint.d/compile-template.sh
server {
    listen       ${LISTEN};
    server_name  ${SERVER_NAME};

location / {
    proxy_pass ${PROXY_PASS};
    proxy_http_version  1.1;
    proxy_cache_bypass  ${DOLLAR}http_upgrade;

    proxy_set_header Upgrade           ${DOLLAR}http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              ${DOLLAR}host;
    proxy_set_header X-Real-IP         ${DOLLAR}remote_addr;
    proxy_set_header X-Forwarded-For   ${DOLLAR}proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto ${DOLLAR}scheme;
    proxy_set_header X-Forwarded-Host  ${DOLLAR}host;
    proxy_set_header X-Forwarded-Port  ${DOLLAR}server_port;
  }
}